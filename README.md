# RosaeNLG Java Wrapper

RosaeNLG is a Natural Language Generation library.

Java wrapper around [RosaeNLG](https://rosaenlg.org).

## Documentation

For documentation, see:
- [RosaeNLG documentation](https://rosaenlg.org)
- [JavaDoc](https://www.javadoc.io/doc/org.rosaenlg/java-wrapper/)
- and [here](doc/modules/java-wrapper/java-wrapper.adoc)


## Contrib

(this part of doc is just for me)

- test packaging skipping tests: `mvn package -Dmaven.test.skip=true`
- upload to central repo: `mvn deploy`

In case of issues with the download cache:
```xml
<overwrite>true</overwrite>
<skipCache>true</skipCache>
```
