package org.rosaenlg.lib;

/*-
 * #%L
 * RosaeNLG for Java
 * %%
 * Copyright (C) 2019 RosaeNLG.org, Ludan Stoecklé
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;

import org.apache.commons.io.FileUtils;

import org.graalvm.polyglot.Value;

import org.json.JSONArray;
import org.json.JSONObject;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestWrapper {

  private static final Logger logger = LoggerFactory.getLogger(TestWrapper.class);

  private static Wrapper wrapper_fr_FR;
  private static Wrapper wrapper_en_US;
  private static String tutoTemplate;

  static {
    try {
      tutoTemplate = FileUtils.readFileToString(
          new File("test-templates/tutorial_en_US.pug"), 
          "utf-8");
      wrapper_fr_FR = new Wrapper("fr_FR");
      wrapper_en_US = new Wrapper("en_US");
    } catch (Exception e) {
      logger.error(e.toString());
    }
  }

  @AfterAll
  public static void finish() throws Exception {
    wrapper_fr_FR.closeContext();
    wrapper_en_US.closeContext();
  }

  @Test
  public void testCompileJsonPackage() throws Exception {

    // 1. compile
    String jsonPackage = FileUtils.readFileToString(
        new File("test-templates/withIncNoComp.json"), 
        "utf-8");

    Value compiled = wrapper_en_US.compileFromJson(jsonPackage);

    // System.out.println(compiled.toString());
    assertNotNull(compiled);
    // NB: Value.toString() only gives the beginning of the code
    assertTrue(compiled.toString().length() > 20);
    assertTrue(compiled.toString().contains("function template(locals)"));
    assertTrue(compiled.toString().contains("var pug_html = "));

    // 2. render
    CompileOptions optsRender = new CompileOptions();
    optsRender.setLanguage("en_US");
    String rendered = wrapper_en_US.runCompiled(compiled, optsRender.toJson());

    assertNotNull(rendered);
    assertEquals(rendered, "<p>Bla included</p>");
  }

  @Test
  public void testRunJsonPackage() throws Exception {

    String jsonPackageAsString = FileUtils.readFileToString(
        new File("test-templates/withIncComp.json"), 
        "utf-8");

    JSONObject jsonPackage = new JSONObject(jsonPackageAsString);
    String compiledString = jsonPackage.getString("compiled");

    CompileOptions optsRender = new CompileOptions();
    optsRender.setLanguage("en_US");
    String rendered = wrapper_en_US.runCompiledClient(
        compiledString, 
        "template", 
        optsRender.toJson());

    assertNotNull(rendered);
    assertEquals(rendered, "<p>Bla included</p>");
  }

  @Test
  public void testCompileFileThenRender() throws Exception {

    // 1. compile
    CompileOptions optsCompile = new CompileOptions();
    optsCompile.setLanguage("en_US");

    Value compiled = wrapper_en_US.compileFile(new File("test-templates", "tutorial_en_US.pug"),
        new File("test-templates"), optsCompile.toJson());

    // System.out.println(compiled.toString());
    assertNotNull(compiled);
    // NB: Value.toString() only gives the beginning of the code
    assertTrue(compiled.toString().length() > 20);
    assertTrue(compiled.toString().contains("function template(locals)"));
    assertTrue(compiled.toString().contains("var pug_html = "));

    // 2. render
    CompileOptions optsRender = new CompileOptions();
    optsRender.setLanguage("en_US");
    String rendered = wrapper_en_US.runCompiled(compiled, optsRender.toJson());

    // System.out.println("RENDERED: " + rendered);

    assertNotNull(rendered);
    assertTrue(rendered.length() > 20);
    assertTrue(rendered.contains("battery"));
    assertTrue(rendered.contains("ratio"));
    assertTrue(rendered.contains(". "));
    assertTrue(rendered.contains("display"));
  }

  @Test
  public void testCompileThenRender() throws Exception {

    // 1. compile
    CompileOptions optsCompile = new CompileOptions();
    optsCompile.setLanguage("en_US");

    Value compiled = wrapper_en_US.compile(tutoTemplate, optsCompile.toJson());
    // System.out.println(compiled.toString());
    assertNotNull(compiled);
    // NB: Value.toString() only gives the beginning of the code
    assertTrue(compiled.toString().length() > 20);
    assertTrue(compiled.toString().contains("function template(locals)"));
    assertTrue(compiled.toString().contains("var pug_html = "));

    // 2. render
    RenderOptions optsRender = new RenderOptions();
    optsRender.setLanguage("en_US");
    String rendered = wrapper_en_US.runCompiled(compiled, optsRender.toJson());

    // System.out.println("RENDERED: " + rendered);

    assertNotNull(rendered);
    assertTrue(rendered.length() > 20);
    assertTrue(rendered.contains("battery"));
    assertTrue(rendered.contains("ratio"));
    assertTrue(rendered.contains(". "));
    assertTrue(rendered.contains("display"));

  }

  @Test
  public void testRenderError() throws Exception {
    try {
      CombinedOptions opts = new CombinedOptions();
      opts.setLanguage("en_US");
      String rendered = wrapper_en_US.renderFile(new File("test-templates", "render_errors.pug"),
          new File("test-templates"), opts.toJson());
      fail("Exception did not throw! " + rendered);
    } catch (Exception e) {
      // System.out.println(e.getMessage());
      assertTrue(e.getMessage().contains("TypeError: value not possible on: true on line 5"));
    }
  }

  @Test
  public void testCompileError() throws Exception {
    try {
      CombinedOptions opts = new CombinedOptions();
      opts.setLanguage("en_US");
      String compiled = wrapper_en_US.compileFileClient(
          new File("test-templates", "comp_errors.pug"),
          new File("test-templates"), opts.toJson());
      fail("Exception did not throw! " + compiled);
    } catch (Exception e) {
      // System.out.println(e.getMessage());
      assertTrue(e.getMessage().contains("Unexpected token"));
      assertTrue(e.getMessage().contains("  > 5|   if true +!= false"));
      assertTrue(e.getMessage().contains("------------------^"));
    }
  }

  @Test
  public void testCompileFileClientWithInclude() throws Exception {
    CompileOptions opts = new CompileOptions();
    opts.setLanguage("en_US");

    String compiled = wrapper_en_US.compileFileClient(
        new File("test-templates/includes", "test.pug"),
        new File("test-templates/includes"),
        opts.toJson());

    // System.out.println(compiled);
    assertNotNull(compiled);
    assertTrue(compiled.length() > 5);
    assertTrue(compiled.contains("bla"));
    assertTrue(compiled.contains("included"));
  }

  @Test
  public void testRenderWithInclude() throws Exception {
    CombinedOptions opts = new CombinedOptions();
    opts.setLanguage("en_US");

    String rendered = wrapper_en_US.renderFile(new File("test-templates/includes", "test.pug"),
        new File("test-templates/includes"), opts.toJson());

    assertNotNull(rendered);
    assertEquals(rendered, "<p>Bla included</p>");
  }

  @Test
  public void testCompileClientThenRender() throws Exception {

    Wrapper wrapperForCompile = new Wrapper("en_US");
    CompileOptions optsComp = new CompileOptions();
    optsComp.setLanguage("en_US").setCompileDebug(false).setName("TheTemplate");
    String compiled = wrapperForCompile.compileClient(tutoTemplate, optsComp.toJson());

    // System.out.println("COMPILED: " + compiled);

    Wrapper wrapperForRender = new Wrapper("en_US");
    RenderOptions optsRender = new RenderOptions();
    optsRender.setLanguage("en_US");
    String rendered = wrapperForRender.runCompiledClient(
        compiled, 
        "TheTemplate", 
        optsRender.toJson());

    // System.out.println("RENDERED: " + rendered);

    assertNotNull(rendered);
    assertTrue(rendered.length() > 20);
    assertTrue(rendered.contains("battery"));
    assertTrue(rendered.contains("ratio"));
    assertTrue(rendered.contains(". "));
    assertTrue(rendered.contains("display"));
  }

  private long runXTimesHelper(String template, String options, int maxIter) throws Exception {
    long start = System.currentTimeMillis();
    for (int i = 1; i <= maxIter; i++) {
      wrapper_en_US.render(template, options);
    }
    return (System.currentTimeMillis() - start) / maxIter;
  }

  @Test
  public void testPugCache() throws Exception {
    final int maxIter = 10;

    CombinedOptions optsNoCache = new CombinedOptions();
    optsNoCache.setLanguage("en_US");
    final long tookNoCache = runXTimesHelper(tutoTemplate, optsNoCache.toJson(), maxIter);

    CombinedOptions optsCache = new CombinedOptions();
    optsCache.setLanguage("en_US");
    optsCache.getCompileOptions().setFilename("cacheKey").setCache(true);

    wrapper_en_US.render(tutoTemplate, optsCache.toJson()); // just once to trigger cache
    final long tookCache = runXTimesHelper(tutoTemplate, optsCache.toJson(), maxIter);

    assertTrue(tookNoCache > tookCache * 1.5,
        "without cache, took: " + tookNoCache + " ms, with cache, took: " + tookCache + " ms");

  }

  @Test
  public void testException() throws Exception {

    try {
      CombinedOptions opts = new CombinedOptions();
      opts.setLanguage("en_US");
      String template = "l #[+value(true)]";

      wrapper_en_US.render(template, opts.toJson());
      fail("Exception did not throw!");
    } catch (Exception e) {
      // System.out.println(e.getMessage());
      assertTrue(e.getMessage().contains("TypeError"));
      assertTrue(e.getMessage().contains("value not possible"));
    }
  }

  @Test
  public void testRenderFrenchEmbed() throws Exception {
    CombinedOptions opts = new CombinedOptions();
    opts.setLanguage("fr_FR");

    String template = FileUtils.readFileToString(new File("test-templates/verb_fr.pug"), "utf-8");
    String rendered = wrapper_fr_FR.render(template, opts.toJson());

    // System.out.println(rendered);
    assertNotNull(rendered);
    assertTrue(rendered.length() > 5);
    assertTrue(rendered.contains("chantera"));
  }

  private static String getJsonPhone1() {
    JSONObject jo = new JSONObject();
    jo.put("name", "OnePlus 5T");
    JSONArray colors = new JSONArray();
    colors.put("Black").put("Red").put("White");
    jo.put("colors", colors);
    jo.put("displaySize", 6);
    jo.put("screenRatio", 80.43);
    jo.put("battery", 3300);

    JSONObject joWrapper = new JSONObject();
    joWrapper.put("language", "en_US");
    joWrapper.put("phone", jo);

    return joWrapper.toString();
  }

  private static String getJsonPhone2() {
    JSONObject jo = new JSONObject();
    jo.put("name", "OnePlus 5");
    JSONArray colors = new JSONArray();
    colors.put("Gold").put("Gray");
    jo.put("colors", colors);
    jo.put("displaySize", 5.5);
    jo.put("screenRatio", 72.93);
    jo.put("battery", 3300);

    JSONObject joWrapper = new JSONObject();
    joWrapper.put("language", "en_US");
    joWrapper.put("phone", jo);

    return joWrapper.toString();
  }

  private static String getJsonPhone3() {
    JSONObject jo = new JSONObject();
    jo.put("name", "OnePlus 3T");
    JSONArray colors = new JSONArray();
    colors.put("Black").put("Gold").put("Gray");
    jo.put("colors", colors);
    jo.put("displaySize", 5.5);
    jo.put("screenRatio", 73.14);
    jo.put("battery", 3400);

    JSONObject joWrapper = new JSONObject();
    joWrapper.put("language", "en_US");
    joWrapper.put("phone", jo);

    return joWrapper.toString();
  }

  @Test
  public void testRenderWithDynamicData() throws Exception {

    String tutoTemplateNoData = FileUtils.readFileToString(
        new File("test-templates/tutorial_en_US_nodata.pug"),
        "utf-8");

    String rendered1 = wrapper_en_US.render(tutoTemplateNoData, getJsonPhone1());
    // System.out.println(rendered1);
    assertTrue(rendered1.contains("OnePlus 5T"));
    assertTrue(rendered1.contains("80.43"));

    String rendered2 = wrapper_en_US.render(tutoTemplateNoData, getJsonPhone2());
    // System.out.println(rendered2);
    assertTrue(rendered2.contains("OnePlus 5"));
    assertTrue(rendered2.contains("5.5"));

    String rendered3 = wrapper_en_US.render(tutoTemplateNoData, getJsonPhone3());
    // System.out.println(rendered3);
    assertTrue(rendered3.contains("OnePlus 3T"));
    assertTrue(rendered3.contains("73.14"));
    assertTrue(rendered3.contains("Black"));
  }

  @Test
  public void testRender() throws Exception {
    CombinedOptions opts = new CombinedOptions();
    opts.setLanguage("en_US");
    String rendered = wrapper_en_US.render(tutoTemplate, opts.toJson());

    // System.out.println(rendered);
    assertNotNull(rendered);
    assertTrue(rendered.length() > 20);
    assertTrue(rendered.contains("battery"));
    assertTrue(rendered.contains("ratio"));
    assertTrue(rendered.contains(". "));
    assertTrue(rendered.contains("display"));
  }

  @Test
  public void testCompileClient() throws Exception {
    CompileOptions opts = new CompileOptions();
    opts.setLanguage("en_US").setCompileDebug(false);

    String compiled = wrapper_en_US.compileClient(tutoTemplate, opts.toJson());

    // System.out.println(compiled);
    assertNotNull(compiled);
    assertTrue(compiled.length() > 20);
    assertTrue(compiled.contains("pug_html = pug_html + "));
    assertTrue(compiled.contains("pug_mixins["));
    assertTrue(compiled.contains("pug_interp"));
    assertTrue(compiled.contains(" function synHelper"));
  }

  @Test
  public void testChansonWithRunCompiled() throws Exception {
    String jsonPackage = FileUtils.readFileToString(
        new File("test-templates/chanson.json"), 
        "utf-8");
    Value compiled = wrapper_fr_FR.compileFromJson(jsonPackage);
    assertNotNull(compiled);

    JSONObject opts = new JSONObject();
    opts.put("language", "fr_FR");
    JSONObject chanson = new JSONObject();
    chanson.put("nom", "Non, je ne regrette rien");
    chanson.put("auteur", "Édith Piaf");
    opts.put("chanson", chanson);

    String rendered = wrapper_fr_FR.runCompiled(compiled, opts.toString());

    assertTrue(rendered.contains("Il chantera \"Non, je ne regrette rien\" d'Édith Piaf"));
  }

  @Test
  public void testNoLanguage() throws Exception {

    CompileOptions opts = new CompileOptions();
    assertThrows(Exception.class, () -> {
      wrapper_en_US.render(tutoTemplate, opts.toJson());
    });

  }

  @Test
  public void testCompileJsonPackageProblem() throws Exception {

    // load
    String jsonPackage = FileUtils.readFileToString(
        new File("test-templates/verb_fr.json"),
        "utf-8")
        .replace("} )]", "} )");

    assertThrows(Exception.class, () -> {
      wrapper_fr_FR.compileFromJson(jsonPackage);
    });
  }

  @Test
  public void testCompileClientException() throws Exception {
    CompileOptions opts = new CompileOptions();
    opts.setLanguage("en_US").setCompileDebug(false);

    String hackedTemplate = tutoTemplate.replace("'tones')]", "'tones')");
    
    assertThrows(Exception.class, () -> {
      wrapper_en_US.compileClient(hackedTemplate, opts.toJson());
    });
  }

  @Test
  public void testCompileException() throws Exception {
    CompileOptions opts = new CompileOptions();
    opts.setLanguage("en_US");

    String hackedTemplate = tutoTemplate.replace("'tones')]", "'tones')");
    
    assertThrows(Exception.class, () -> {
      wrapper_en_US.compile(hackedTemplate, opts.toJson());
    });
  }
  
  @Test
  public void testCompileJsonPackageWithParams() throws Exception {

    // 1. compile
    String jsonPackage = FileUtils.readFileToString(
        new File("test-templates/verb_fr.json"), 
        "utf-8");

    Value compiled = wrapper_fr_FR.compileFromJson(jsonPackage);

    // 2. render
    RenderOptions optsRender = new RenderOptions();
    optsRender.setLanguage("fr_FR")
      .setDefaultAmong(36)
      .setDefaultSynoMode("random")
      .setDisableFiltering(false)
      .setForceRandomSeed(1);

    String rendered = wrapper_fr_FR.runCompiled(compiled, optsRender.toJson());

    assertNotNull(rendered);
    assertEquals(rendered, "<p>Il chantera</p>");
  }

  @Test
  public void testCompileFileException() throws Exception {
    CompileOptions opts = new CompileOptions();
    opts.setLanguage("fr_FR");
    
    assertThrows(Exception.class, () -> {
      wrapper_fr_FR.compileFile(
          new File("test-templates", "verb_fr_error.pug"),
          new File("test-templates"), 
          opts.toJson());
    });
  }

  @Test
  public void testRunCompiledProblem() throws Exception {

    // load
    String jsonPackage = FileUtils.readFileToString(
        new File("test-templates/verb_fr.json"), 
        "utf-8");

    // hack
    jsonPackage = jsonPackage.replace("+verb", "+BLABLA");

    Value compiled = wrapper_fr_FR.compileFromJson(jsonPackage);

    RenderOptions optsRender = new RenderOptions();
    optsRender.setLanguage("fr_FR");
    
    assertThrows(Exception.class, () -> {
      wrapper_fr_FR.runCompiled(compiled, optsRender.toJson());
    });    
  }

  @Test
  public void testRunCompiledClientProblem() throws Exception {

    // load
    String template = FileUtils.readFileToString(
        new File("test-templates/verb_fr.pug"), 
        "utf-8");

    // hack
    template = template.replace("+verb", "+BLABLA");

    CompileOptions optsComp = new CompileOptions();
    optsComp.setLanguage("fr_FR");
    String compiled = wrapper_fr_FR.compileClient(template, optsComp.toJson());

    RenderOptions optsRender = new RenderOptions();
    optsRender.setLanguage("fr_FR");
    
    assertThrows(Exception.class, () -> {
      wrapper_fr_FR.runCompiledClient(compiled, "template", optsRender.toJson());
    });    
  }

}
