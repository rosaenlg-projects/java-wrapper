package org.rosaenlg.lib;

/*-
 * #%L
 * RosaeNLG for Java
 * %%
 * Copyright (C) 2019 RosaeNLG.org, Ludan Stoecklé
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import org.json.JSONObject;

/**
 * RosaeNLG compilation and rendering options helper.
 * <p>
 * <a href="https://rosaenlg.org/rosaenlg/1.4.0/advanced/params.html">Compile and render options</a>
 * </p>
 * @author Ludan Stoecklé ludan.stoeckle@rosaenlg.org
 */
public class CombinedOptions {

  private CompileOptions compileOptions = new CompileOptions();
  private RenderOptions renderOptions = new RenderOptions();

  /**
   * Serializes the object to a JSON String.
   * 
   * @return String the object as a JSON String
   */
  public String toJson() {
    JSONObject merged = new JSONObject(compileOptions.toJson());

    JSONObject renderJson = new JSONObject(renderOptions.toJson());
    for (String key : JSONObject.getNames(renderJson)) {
      merged.put(key, renderJson.get(key));
    }

    return merged.toString();
  }

  /**
   * Sets the language, for instance 'en_US' or 'fr_FR'.
   * 
   * @param language the language
   */
  public void setLanguage(String language) {
    this.compileOptions.setLanguage(language);
    this.renderOptions.setLanguage(language);
  }

  /**
   * Getter on compile options.
   * 
   * @return CompileOptions
   */
  public CompileOptions getCompileOptions() {
    return this.compileOptions;
  }

  /**
   * Getter on render options.
   * 
   * @return RenderOptions
   */
  public RenderOptions getRenderOptions() {
    return this.renderOptions;
  }
}
