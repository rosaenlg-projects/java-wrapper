package org.rosaenlg.lib;

/*-
 * #%L
 * RosaeNLG for Java
 * %%
 * Copyright (C) 2019 RosaeNLG.org, Ludan Stoecklé
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.FileSystems;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Source;
import org.graalvm.polyglot.Value;

import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * Wrapper around RosaeNLG.
 * 
 * @see <a href="https://rosaenlg.org">RosaeNLG documentation</a>
 * @author Ludan Stoecklé ludan.stoeckle@rosaenlg.org
 */
public class Wrapper {

  private static final Logger logger = LoggerFactory.getLogger(Wrapper.class);

  private Context context;

  private static Map<String, Source> sourcesRosaeNLG = new HashMap<String, Source>();
  private static Source sourceWrapper;

  private String language;

  private Value renderFct;
  private Value renderFileFct;
  private Value compileFct;
  private Value compileFileFct;
  private Value compileClientFct;
  private Value compileFileClientFct;
  private Value runCompiledClientFct;

  private static final String exceptionMarker = "EXCEPTION ";

  static {
    logger.info("Entering static constructor...");

    try {
      sourceWrapper = Source.newBuilder("js",
          new InputStreamReader(
            Wrapper.class.getResourceAsStream("/wrapper.js"), "UTF-8"), 
            "wrapper.js").build();
      try {
        // System.out.println("We are in a fat jar...");
        URL res = Wrapper.class.getClassLoader().getResource("/META-INF/truffle/language");
        // initialize the file system for the language file
        FileSystems.newFileSystem(res.toURI(), new HashMap<>());
        logger.info("Fat jar FileSystem ok");
      } catch (Throwable ignored) {
        // in case of starting without fat jar
      }

    } catch (Exception e) {
      throw new RuntimeException("cannot find wrapper.js: " + e.toString());
    }

    logger.info("Constructor static done.");
  }

  /**
   * Creates a wrapper from a language.
   * <p>
   * Wrapper is used to compiled, render etc.
   * </p>
   * 
   * @param language valid RosaeNLG language (fr_FR, en_US, de_DE, it_IT, OTHER)
   * @throws Exception if a problem occurs
   */
  public Wrapper(String language) throws Exception {
    logger.info("Entering constructor, language is {}...", language);

    this.language = language;

    // context = Context.create();
    context = Context.newBuilder("js").allowAllAccess(false).build();

    if (sourcesRosaeNLG.get(language) == null) {
      logger.info("will now load rosaenlg js for {}", language);

      final Properties properties = new Properties();
      properties.load(Wrapper.class.getResourceAsStream("/project.properties"));
      String version = properties.getProperty("rosaenlg.version");
      String rosaejsFileName = "rosaenlg_tiny_" + language + "_" + version + "_comp.js";
      logger.info("using rosaenlg file: ", rosaejsFileName);

      Source sourceRosaeNlg = Source.newBuilder(
          "js",
          new InputStreamReader(
              Wrapper.class.getResourceAsStream("/" + rosaejsFileName), "UTF-8"), 
              rosaejsFileName)
          .build();

      sourcesRosaeNLG.put(language, sourceRosaeNlg);
      logger.info("RosaeNLG js lib read, lines {}", sourceRosaeNlg.getLineCount());
    } else {
      logger.info("rosaenlg js for {} is already loaded", language);
    }

    context.eval(sourcesRosaeNLG.get(language));
    context.eval(sourceWrapper);

    renderFct = context.eval("js", "render");
    assert renderFct.canExecute();

    renderFileFct = context.eval("js", "renderFile");
    assert renderFileFct.canExecute();

    compileFct = context.eval("js", "compile");
    assert compileFct.canExecute();

    compileFileFct = context.eval("js", "compileFile");
    assert compileFileFct.canExecute();

    compileClientFct = context.eval("js", "compileClient");
    assert compileClientFct.canExecute();

    compileFileClientFct = context.eval("js", "compileFileClient");
    assert compileFileClientFct.canExecute();

    runCompiledClientFct = context.eval("js", "runCompiledClient");
    assert runCompiledClientFct.canExecute();

    logger.info("Constructor done.");
  }

  /**
   * Close the underlying GraalVM Context.
   * <p>
   * For cleanup.
   * </p>
   * 
   * @throws Exception if the context cannot be closed
   */
  public void closeContext() throws Exception {
    logger.info("Starting finalize...");
    context.close(false); // no cancel if still executing
    logger.info("Finalize done.");
  }

  /**
   * Renders a template under a String form.
   * <p>
   * Takes a template (String), and options (String containing json). It is not so
   * useful, because often templates are spread accross multiple files with Pug
   * includes https://pugjs.org/language/includes.html. So 'renderFile' is more
   * useful. To make the Pug cache work, you must put 'cache' option to 'true' in
   * options and also put a key (that you choose) so that pug recognizes that it
   * is the same template using 'filename' option.
   * </p>
   * 
   * @param source      contains the template as a String
   * @param jsonOptions options in JSON form
   * @return String the rendered template
   * @throws Exception if a problem occurs
   */
  public String render(String source, String jsonOptions) throws Exception {
    String rendered = renderFct.execute(
        source, 
        this.language, 
        jsonOptions, 
        exceptionMarker).asString();
    if (rendered.startsWith(exceptionMarker)) {
      throw new Exception(rendered.replace(exceptionMarker, ""));
    }
    return rendered;
  }

  private Map<String, String> getAllFilesIn(File path) throws IOException {
    Map<String, String> res = new HashMap<String, String>();
    Collection<File> files = FileUtils.listFiles(
        path, 
        TrueFileFilter.INSTANCE, 
        TrueFileFilter.INSTANCE);
    for (File file : files) {
      String content = FileUtils.readFileToString(file, "utf-8");
      res.put(file.toString().replace("\\", "/"), // must fit pug convention
          content);
      // System.out.println(file.toString());
    }
    // System.out.println(res.toString());
    return res;
  }

  
  private String getAllFilesInAsJson(File path) throws IOException {
    Map<String, String> filesWithContent = this.getAllFilesIn(path);
    return (new JSONObject(filesWithContent)).toString();
  }

  /**
   * Renders a template on the disk, supporting Pug includes.
   * <p>
   * This is the most useful method generally. The template is read on the disk:
   * its path is the first parameter. Pug includes are automatically read but you
   * must provide the includesPath folder.
   * </p>
   * 
   * @param template     path to template
   * @param includesPath path to a folder containing potential includes
   * @param jsonOptions  options in JSON form
   * @return String the rendered template
   * @throws Exception if a problem occurs
   */
  public String renderFile(File template, File includesPath, String jsonOptions) throws Exception {
    String rendered = renderFileFct.execute(
        template.toString(), 
        this.language, 
        this.getAllFilesInAsJson(includesPath),
        jsonOptions, exceptionMarker).asString();
    if (rendered.startsWith(exceptionMarker)) {
      throw new Exception(rendered.replace(exceptionMarker, ""));
    }
    return rendered;
  }

  /**
   * Compiles a template on the disk, supporting Pug includes.
   * <p>
   * This is useful to compile a templates, bundle everything for later rendering,
   * typically client side in a browser, or using 'runCompiledClient' method. The
   * template is read on the disk: its path is the first parameter. Pug includes
   * are automatically read but you must provide the 'includesPath' folder. In
   * jsonOptions you can specify the 'name' property, default is 'template'.
   * </p>
   * 
   * @param template     path to template
   * @param includesPath path to a folder containing potential includes
   * @param jsonOptions  options in JSON form
   * @return String the compiled template (which is JavaScript code)
   * @throws Exception if a problem occurs
   */
  public String compileFileClient(
      File template, 
      File includesPath, 
      String jsonOptions) throws Exception {
    String compiled = compileFileClientFct.execute(template.toString(), this.language,
        this.getAllFilesInAsJson(includesPath), jsonOptions, exceptionMarker).asString();
    if (compiled.startsWith(exceptionMarker)) {
      throw new Exception(compiled.replace(exceptionMarker, ""));
    }
    return compiled;
  }

  /**
   * Compiles a template to a JavaScript code string.
   * <p>
   * It does not support pug includes, thus 'compileFileClient' is generally more
   * practical. In jsonOptions you can specify the 'name' property, default is
   * 'template'.
   * </p>
   * 
   * @param source      the Pug template itself
   * @param jsonOptions options in JSON form
   * @return String the compiled template (which is JavaScript code)
   * @throws Exception if a problem occurs
   */
  public String compileClient(String source, String jsonOptions) throws Exception {
    String compiled = compileClientFct.execute(
        source, 
        this.language, 
        jsonOptions, 
        exceptionMarker).asString();
    if (compiled.startsWith(exceptionMarker)) {
      throw new Exception(compiled.replace(exceptionMarker, ""));
    }
    return compiled;
  }

  /**
   * Compiles a template to JavaScript code.
   * <p>
   * It does not support pug includes, thus 'compileFile' is generally more
   * practical. Result can be used directly with 'runCompiled'. In jsonOptions you
   * can specify the 'name' property, default is 'template'.
   * </p>
   * 
   * @param source      the Pug template itself
   * @param jsonOptions options in JSON form
   * @return Value the compiled template (which is JavaScript code already loaded
   *         in the GraalVM Context)
   * @throws Exception if a problem occurs
   */
  public Value compile(String source, String jsonOptions) throws Exception {
    Value compiled = compileFct.execute(source, this.language, jsonOptions, exceptionMarker);
    // System.out.println(compiled.getClass().getName());
    if (compiled.toString().startsWith(exceptionMarker)) {
      throw new Exception(compiled.toString().replace(exceptionMarker, ""));
    }
    return compiled;
  }

  /**
   * Compiles a template on the disk, supporting Pug includes.
   * <p>
   * It is not clear whether this is really useful or not, but you might run the
   * result using 'runCompiled' method. The template is read on the disk: its path
   * is the first parameter. Pug includes are automatically read but you must
   * provide the 'includesPath' folder. In jsonOptions you can specify the 'name'
   * property, default is 'template'.
   * </p>
   * 
   * @param template     path to template
   * @param includesPath path to a folder containing potential includes
   * @param jsonOptions  options in JSON form
   * @return Value the compiled template (which is JavaScript code already loaded
   *         in the GraalVM Context)
   * @throws Exception if a problem occurs
   */
  public Value compileFile(File template, File includesPath, String jsonOptions) throws Exception {
    Value compiled = compileFileFct.execute(
        template.toString(), 
        this.language, 
        this.getAllFilesInAsJson(includesPath),
        jsonOptions, exceptionMarker);
    // System.out.println(compiled.getClass().getName());
    if (compiled.toString().startsWith(exceptionMarker)) {
      throw new Exception(compiled.toString().replace(exceptionMarker, ""));
    }
    return compiled;
  }

  
  /** Compiles a template from a JSON package, already parsed.
   * 
   * @param jsonPackage the parsed Json Package object
   * @return Value the compiled template (which is JavaScript code already loaded)
   * @throws Exception if a problem occurs (in the GraalVM Context)
   */
  public Value compileFromJson(JsonPackage jsonPackage) throws Exception {

    logger.info("templates: {}", (new JSONObject(jsonPackage.getTemplates())).toString());
    logger.info("compileInfo: {}", jsonPackage.getCompileInfo().toJson());

    Value compiled = compileFileFct.execute(
        jsonPackage.getEntryTemplate(), 
        this.language,
        (new JSONObject(jsonPackage.getTemplates())).toString(), 
        jsonPackage.getCompileInfo().toJson(), 
        exceptionMarker);
    
    if (compiled.toString().startsWith(exceptionMarker)) {
      throw new Exception(compiled.toString().replace(exceptionMarker, ""));
    }
    return compiled;
  }


  /**
   * Compiles a template from a JSON package, given as a String.
   * <p>
   * See
   * https://gitlab.com/rosaenlg-projects/rosaenlg/blob/master/packages/gulp-rosaenlg/README.md
   * to create such a package.
   * </p>
   * 
   * @param jsonPackageAsString content of the json package
   * @return Value the compiled template (which is JavaScript code already loaded)
   * @throws Exception if a problem occurs (in the GraalVM Context)
   */
  public Value compileFromJson(String jsonPackageAsString) throws Exception {
    JsonPackage jsonPackage = new JsonPackage(jsonPackageAsString);
    return this.compileFromJson(jsonPackage);
  }

  /**
   * Runs compiled code.
   * <p>
   * Runs code compiled using 'compile' or 'compileFile'. In jsonOptions you can
   * specify the 'name' property, default is 'template'.
   * </p>
   * 
   * @param compiledTemplate    previously compiled template
   * @param jsonOptionsAsString options in JSON form
   * @return String the rendered template
   * @throws Exception if a problem occurs
   */
  public String runCompiled(Value compiledTemplate, String jsonOptionsAsString) throws Exception {

    // inject NlgLib into the options
    JSONObject jsonOptions = new JSONObject(jsonOptionsAsString);

    RenderOptions runtimeOptions = new RenderOptions(jsonOptions);

    // we keep original but add 'util'
    jsonOptions.put("util", "NLGLIB_PLACEHOLDER");
    String finalOptions = jsonOptions.toString().replace("\"NLGLIB_PLACEHOLDER\"",
        "new rosaenlg_"
        + this.language
        + ".NlgLib(JSON.parse('"
        + runtimeOptions.toJson() + "'))");
    String paramForge = "() => { return " + finalOptions + ";}";

    Value realParam = context.eval("js", paramForge).execute();
    String rendered = compiledTemplate.execute(realParam).asString();
    if (rendered.startsWith(exceptionMarker)) {
      throw new Exception(rendered.replace(exceptionMarker, ""));
    }
    return rendered;
  }

  /**
   * Runs compiled code.
   * <p>
   * Runs code compiled using 'compileClient' or 'compileFileClient'. In
   * jsonOptions you can specify the 'name' property, default is 'template'.
   * </p>
   * 
   * @param compiledTemplate     previously compiled template
   * @param templateFunctionName the name of the template (default when compiling
   *                             is 'template' but you must specify it)
   * @param jsonOptions          options in JSON form
   * @return String the rendered template
   * @throws Exception if a problem occurs
   */
  public String runCompiledClient(
      String compiledTemplate, 
      String templateFunctionName, 
      String jsonOptions)
      throws Exception {
    String rendered = runCompiledClientFct.execute(
        compiledTemplate, 
        this.language, 
        templateFunctionName, 
        jsonOptions, 
        exceptionMarker).asString();
    if (rendered.startsWith(exceptionMarker)) {
      throw new Exception(rendered.replace(exceptionMarker, ""));
    }
    return rendered;
  }

}
