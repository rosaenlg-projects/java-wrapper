package org.rosaenlg.lib;

/*-
 * #%L
 * RosaeNLG for Java
 * %%
 * Copyright (C) 2019 RosaeNLG.org, Ludan Stoecklé
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import org.json.JSONObject;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

/**
 * RosaeNLG compilation options helper.
 * <p>
 * <a href="https://rosaenlg.org/rosaenlg/1.4.0/advanced/params.html#_compiling_parameters">Compile options</a>
 * </p>
 * @author Ludan Stoecklé ludan.stoeckle@rosaenlg.org
 */
public class CompileOptions {

  // private static final Logger logger = LoggerFactory.getLogger(CompileOptions.class);

  private String language;
  private String name;
  private Boolean compileDebug;
  private String filename;
  private Boolean cache;

  
  /** Default constructor, does nothing.
   */
  public CompileOptions() {
  }

  
  /** Constructor from json parsed object.
   * 
   * <p>
   * Will find different properties in the json object and populate the object.
   * </p>
   * 
   * @param compileOptionsJson json object containing the properties
   */
  public CompileOptions(JSONObject compileOptionsJson) {
    
    if (compileOptionsJson.has("language")) {
      this.language = compileOptionsJson.getString("language");
    }
    if (compileOptionsJson.has("name")) {
      this.name = compileOptionsJson.getString("name");
    }
    if (compileOptionsJson.has("compileDebug")) {
      this.compileDebug = compileOptionsJson.getBoolean("compileDebug");
    }
    if (compileOptionsJson.has("filename")) {
      this.filename = compileOptionsJson.getString("filename");
    }
    if (compileOptionsJson.has("cache")) {
      this.cache = compileOptionsJson.getBoolean("cache");
    }
  }

  /**
   * Serializes the object to a JSON String.
   * 
   * @return String the object as a JSON String
   */
  public String toJson() {
    JSONObject res = new JSONObject();
    if (this.language != null) {
      res.put("language", this.language);
    }
    if (this.name != null) {
      res.put("name", this.name);
    }
    if (this.compileDebug != null) {
      res.put("compileDebug", this.compileDebug);
    }
    if (this.filename != null) {
      res.put("filename", this.filename);
    }
    if (this.cache != null) {
      res.put("cache", this.cache);
    }
    return res.toString();
  }

  /**
   * Sets the language, for instance 'en_US' or 'fr_FR'.
   * 
   * @param language the language
   * @return this to set further options
   */
  public CompileOptions setLanguage(String language) {
    this.language = language;
    return this;
  }

  /**
   * Sets the name of the output function.
   * <p>
   * This is only useful for 'compileFileClient', 'compileClient' and 'compile'
   * methods.
   * </p>
   * 
   * @param name name of the output function
   * @return this to set further options
   */
  public CompileOptions setName(String name) {
    this.name = name;
    return this;
  }

  /**
   * Activates compile debug Pug option.
   * <p>
   * Is true by default in Pug.
   * </p>
   * 
   * @param compileDebug activates or not compileDebug
   * @return this to set further options
   */
  public CompileOptions setCompileDebug(Boolean compileDebug) {
    this.compileDebug = compileDebug;
    return this;
  }

  /**
   * Provides the key for the Pug cache to work when not compiling files.
   * <p>
   * When using 'render' and not 'renderFile', if you want the cache to work, you
   * must provide a key. It can be anything, it is not related to a real file.
   * </p>
   * 
   * @param filename the key in the cache
   * @return this to set further options
   */
  public CompileOptions setFilename(String filename) {
    this.filename = filename;
    return this;
  }

  /**
   * Activates Pug cache.
   * <p>
   * Activates Pug cache to avoid to compilate again when rendering the same
   * template again and again. Pug default is false.
   * </p>
   * 
   * @param cache true to activate the cache
   * @return this to set further options
   */
  public CompileOptions setCache(Boolean cache) {
    this.cache = cache;
    return this;
  }

  /**
   * Returns the language.
   * 
   * @return language
   */  
  public String getLanguage() {
    return this.language;
  }
}
